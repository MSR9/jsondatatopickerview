//
//  ViewController.swift
//  JSONDataToPickerView
//
//  Created by EPITADMBP04 on 3/26/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import UIKit

struct Country: Decodable {
    var name : String
}

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
  
    /// IBOutlets
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var pickerView: UIPickerView!
    
    @IBOutlet weak var backview: UIView!
    
    
    var countries = [Country]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        
        let url = URL(string: "https://restcountries.eu/rest/v2/all")
        if let urlString = url {
            URLSession.shared.dataTask(with: urlString) { (data, response, error) in
                if error == nil {
                    do {
                        self.countries = try JSONDecoder().decode([Country].self, from: data!)
                    } catch {
                        print("Parse Error")
                    }
                    DispatchQueue.main.async {
                        self.pickerView.reloadComponent(0)
                    }
                }
            }.resume()
        }
    }
    
    // MARK: - PickerView Methods
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countries.count
      }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return countries[row].name
    }
    
    /// Delegate Method
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let selectedCountry = countries[row].name
        self.label?.text =  selectedCountry
    }
}

